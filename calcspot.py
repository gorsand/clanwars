#!/usr/bin/env python

import math
import random

# levels = [ 400, 600, 800, 900, 950, 980, 1000, 1100, 1300, 1500]

levels = [ 500, 700, 800, 900, 1000, 1100, 1200]

with_tiredness = True

def tiredness_coef(nfights):
    if nfights == 0:
        return 1.0
    if nfights == 1:
        return 0.9
    if nfights == 2:
        return 0.85
    if nfights == 3:
        return 0.8
    return 0.5

def health_after(health, damage):
    if health <= damage:
        return 0
    c = math.sqrt(math.sqrt(health))
    v = math.sqrt(math.sqrt(health - damage))
    return c * c * c *v 

m = levels[-1]
for v in levels:
    r = int(health_after(float(m), float(v)))
    print("{} =x= {} -> {}".format(m, v, r))
print("")

class Player(object):

    def __init__(self, level):
        self.level = level
        self.health = level
        self.nfights = 0

    def copy(self):
        p = Player(self.level)
        p.health = self.health
        p.nfights = self.nfights
        return p

    def effective_health(self):
        if with_tiredness:
            return self.health * tiredness_coef(self.nfights)
        return self.health

    def fight(self, enemy):
        p1 = self.effective_health()
        p2 = enemy.effective_health()
        if p1 > p2:
            self.health = max(1, int(health_after(p1, p2)))
        else:
            self.health = 0
        self.nfights += 1

    def is_dead(self):
        return (self.health == 0)

def choose_winner(conf1, conf2):
    c1 = [Player(x) for x in conf1]
    c2 = [Player(x) for x in conf2]
    while True:
        if not c1:
            return conf2
        if not c2:
            return conf1
        p1 = c1[0].copy()
        p2 = c2[0].copy()
        c1[0].fight(p2)
        c2[0].fight(p1)
        if c1[0].is_dead():
            c1 = c1[1:]
        else:
            c2 = c2[1:]

def modify_conf(conf):
    new = [x for x in conf]
    if len(new) >= 2:
        i = random.randint(0,len(new)-2)
        new[i], new[i+1] = new[i+1], new[i]
    return new

def print_conf(conf):
    for p in conf:
        print(str(p) + "\t" + "#" * (p / 100))
    print("")

best_conf = [ x for x in levels ]
last_best_conf = best_conf

print_each = 10000
max_tries = 1000000

n = 0
while n < max_tries:
    new_conf = modify_conf(best_conf)
    best_conf = choose_winner(best_conf, new_conf)
    n += 1
    if n % print_each == 0:
        if best_conf != last_best_conf:
            print("After {} tries:".format(n))
            print_conf(best_conf)
            last_best_conf = best_conf
